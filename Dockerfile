# Base image
FROM alpine:3

# Install SSL certificates
RUN apk --no-cache --no-progress --update add libc6-compat ca-certificates dumb-init

# Copy bins
COPY bin/sensuctl /usr/local/bin/sensuctl
COPY bin/sensu-backend /usr/local/bin/sensu-backend
COPY bin/sensu-agent /usr/local/bin/sensu-agent

VOLUME ["/var/lib/sensu"]

EXPOSE 2379 2380 3000 8080 8081

# Default command
CMD ["sensu-backend"]
