#!/bin/bash -e

# Clone sensu repository
git clone https://github.com/sensu/sensu-go.git
cd sensu-go
git fetch --all --tags

TOKEN=$(curl -s -H "Content-Type: application/json" -X POST -d '{"username": "'${DOCKERHUB_USER}'", "password": "'${DOCKERHUB_PASSWORD}'"}' https://hub.docker.com/v2/users/login/ | jq -r .token)
DOCKER_TAGS=$(curl -s -H "Authorization: JWT ${TOKEN}" "https://hub.docker.com/v2/repositories/${DOCKER_REPOSITORY}/tags?page_size=10000" | jq -r '.results|.[]|.name')
GIT_TAGS=$(git tag | grep "^v6")

for GIT_TAG in ${GIT_TAGS}; do
  GIT_VERSION="$(echo "${GIT_TAG}" | sed -e 's/^v//g')"

  TAG_EXISTS=0

  for DOCKER_TAG in ${DOCKER_TAGS}; do
    DOCKER_TAG="$(echo "${DOCKER_TAG}" | sed -e 's/^"//g' -e 's/"$//g')"

    [ "${GIT_VERSION}" = "${DOCKER_TAG}" ] && TAG_EXISTS=1
  done

  if [ "${TAG_EXISTS}" -eq 0 ]; then
    git reset --hard
    git checkout "${GIT_TAG}"

    if ! [ -f "go.mod" ]; then
      continue
    fi

    echo "Building version: ${GIT_VERSION}"

    mkdir -p ../bin
    rm -rf ../bin/*

    DATE="$(date +%Y-%m-%d)"
    VERSION="${GIT_VERSION}"
    REVISION="$(git rev-parse HEAD)"

    # Build
    docker run --rm -v $(pwd):/src -v $(pwd)/../bin:/out golang:latest bash -c "cd /src && go build -ldflags='-w -s -X \"github.com/sensu/sensu-go/version.Version='${VERSION}'\" -X \"github.com/sensu/sensu-go/version.BuildDate='${DATE}'\" -X \"github.com/sensu/sensu-go/version.BuildSHA='${REVISION}'\"' -o /out/sensu-agent ./cmd/sensu-agent"
    docker run --rm -v $(pwd):/src -v $(pwd)/../bin:/out golang:latest bash -c "cd /src && go build -ldflags='-w -s -X \"github.com/sensu/sensu-go/version.Version='${VERSION}'\" -X \"github.com/sensu/sensu-go/version.BuildDate='${DATE}'\" -X \"github.com/sensu/sensu-go/version.BuildSHA='${REVISION}'\"' -o /out/sensu-backend ./cmd/sensu-backend"
    docker run --rm -v $(pwd):/src -v $(pwd)/../bin:/out golang:latest bash -c "cd /src && go build -ldflags='-w -s -X \"github.com/sensu/sensu-go/version.Version='${VERSION}'\" -X \"github.com/sensu/sensu-go/version.BuildDate='${DATE}'\" -X \"github.com/sensu/sensu-go/version.BuildSHA='${REVISION}'\"' -o /out/sensuctl ./cmd/sensuctl"

    cd ..

    docker build -t "${DOCKER_REPOSITORY}:${VERSION}" .
    docker push "${DOCKER_REPOSITORY}:${VERSION}"

    cd sensu-go

    LATEST_TAG="${VERSION}"
  fi
done

if ! [ -z "${LATEST_TAG}" ]; then
  echo "Tagging ${VERSION} as latest"
  docker tag "${DOCKER_REPOSITORY}:${VERSION}" "${DOCKER_REPOSITORY}:latest"
  docker push "${DOCKER_REPOSITORY}:latest"
fi
